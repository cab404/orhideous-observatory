package com.cab404.guidy;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Font loader
 *
 * @author cab404
 */
public class FL {

    public static final int DEFAULT = 0;

    // Символы, загружаемые из шрифта.
    public static final String chars =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    + "abcdefghijklmnopqrstuvwxyz"
                    + "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
                    + "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
                    + "1234567890-=#!\"№;%:?*()_+\\/|<>.,'[]{}";

    private static FL SINGLETON;

    public static FL getInstance() {
        if (SINGLETON == null)
            SINGLETON = new FL();
        return SINGLETON;
    }

    public static void reset() {
        if (SINGLETON != null)
            for (Map.Entry<Integer, BitmapFont> e : SINGLETON.cache.entrySet()) {
                if (e.getValue() != null) e.getValue().dispose();
            }

    }

    private Map<Integer, BitmapFont> cache;

    private FL() {
        cache = new HashMap<>();
    }

    public BitmapFont loadFont(FileHandle path, int id, int size) {

        if (cache.containsKey(id))
            return cache.get(id);

        return reloadFont(path, id, size);
    }

    public BitmapFont reloadFont(FileHandle path, int id, int size) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(path);
        BitmapFont font = generator.generateFont(size, chars, false);
        cache.put(id, font);
//        Luna.log(String.format("Font #%s cached.", id));
        return font;
    }

    public BitmapFont font(int id) {
        if (!cache.containsKey(id))
            throw new NoSuchElementException(String.format("Font #%s not found!", id));
        return cache.get(id);
    }

    public BitmapFont def() {
        return font(DEFAULT);
    }

    @Override protected void finalize()
    throws Throwable {
        super.finalize();

        for (Map.Entry<Integer, BitmapFont> e : cache.entrySet()) {
            if (e.getValue() != null) e.getValue().dispose();
        }

    }
}
