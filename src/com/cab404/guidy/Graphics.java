package com.cab404.guidy;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Simple class that handles coordinate translation between drawing layers.
 *
 * @author cab404
 */
public class Graphics {
    public SpriteBatch batch;
    public OrthographicCamera cam;

    public Graphics() {
        batch = new SpriteBatch();
        cam = new OrthographicCamera(Luna.screen().x, Luna.screen().y);

        update();
    }

    /**
     * Returns camera position
     */
    public Vector2 cam_pos() {
        return convertToGFX(new Vector2(cam.position.x, cam.position.y));
    }

    /**
     * Returns camera size
     */
    public Vector2 cam_size() {
        return new Vector2(cam.viewportWidth, cam.viewportHeight).scl(cam.zoom);
    }

    /**
     * Returns BB of the camera
     */
    public Rectangle getCameraBounds() {
        Vector2 pos = cam_pos();
        Vector2 size = cam_size();
        return new Rectangle(pos.x, pos.y, size.x, size.y);
    }

    /**
     * Sets camera up.
     *
     * @param pos  Center coordinates in global pane
     * @param lbc  Left-bottom corner of camera
     * @param ruc  Right-upper corner of camera
     * @param zoom Camera zoom
     */
    public Vector2 focusOn(Vector2 pos, Vector2 lbc, Vector2 ruc, float zoom) {
        Vector2 point = pos.cpy();
        float hw = cam.viewportWidth / 2 / zoom, hh = cam.viewportHeight / 2 / zoom;
        point.sub(hw, hh);

        point.x = (point.x + hw) < lbc.x + hw ? lbc.x : ((point.x + hw * 2) > ruc.x ? ruc.x - hw * 2 : point.x);
        point.y = (point.y + hh) < lbc.y + hh ? lbc.y : ((point.y + hh * 2) > ruc.y ? ruc.y - hh * 2 : point.y);

        cam.position.set(point.x, point.y, 0);

        cam.zoom = 1f / zoom;
        point.add(hw, hh);

        update();
        return point;
    }

    /**
     * Updates camera size and translation.
     */
    public void update() {
        Vector2 scr = Luna.screen();
        cam.viewportWidth = scr.x;
        cam.viewportHeight = scr.y;
        cam.update();
        batch.setProjectionMatrix(cam.projection);
        // Я не знаю, кагого Дискорда у камеры 0:0 в центре по умолчанию, но нафиг.
        batch.getProjectionMatrix().translate(
                -scr.x / 2 * cam.zoom,
                -scr.y / 2 * cam.zoom,
                0);
        batch.setTransformMatrix(cam.view);
    }

    /**
     * Converts coordinates from global (camera position on 0:0, zoom is 1) to this graphics.
     *
     * @see Graphics#convertFromGFX(com.badlogic.gdx.math.Vector2)
     */
    public Vector2 convertToGFX(Vector2 in) {
        return in.cpy()
                .scl(cam.zoom)
                .add(cam.position.x, cam.position.y)
                ;
    }

    /**
     * Converts coordinates from this graphics to global.
     *
     * @see Graphics#convertToGFX(com.badlogic.gdx.math.Vector2)
     */
    public Vector2 convertFromGFX(Vector2 in) {
        return in.cpy()
                .sub(cam.position.x, cam.position.y)
                .div(cam.zoom)
                ;
    }

    public Vector2 convertTo(Graphics gfx, Vector2 in) {
        return convertToGFX(gfx.convertFromGFX(in));
    }
}
