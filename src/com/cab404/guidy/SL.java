package com.cab404.guidy;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.util.HashMap;

/**
 * SpriteLoader
 *
 * @author cab404
 */
public class SL {
    private HashMap<String, Texture> sprites;

    public SL() {
        sprites = new HashMap<>();
    }

    public Sprite load(String path) {
        if (!sprites.containsKey(path)) {
            sprites.put(path, new Texture(path));
        }
        return new Sprite(sprites.get(path));
    }

    public void dispose() {
        for (Texture texture : sprites.values()) {
            texture.dispose();
        }
    }


    private static SL instance;
    public static SL inst() {
        if (instance == null)
            instance = new SL();
        return instance;
    }

    @Override protected void finalize()
    throws Throwable {
        super.finalize();
        dispose();
    }
}
