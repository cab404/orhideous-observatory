package com.cab404.neurogame.settings;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Saves settings.
 *
 * @author cab404
 */
public class SettingsSaver {

    private File settings_dir;

    public SettingsSaver(File settings_dir) {
        this.settings_dir = settings_dir;
    }

    private File get_data_file(String namespace) {
        return new File(settings_dir.getAbsolutePath() + File.separatorChar + namespace);
    }

    private class KV {
        String k, v;
        private KV(String k, String v) {
            this.k = k;
            this.v = v;
        }
    }

    private final static String[][] escapes = {
            {"$", "$d"},
            {":", "$c"},
            {"\n", "$n"},
            {"\r", "$r"},
            {" ", "$s"},
    };

    private String escape(String in) {

        for (int i = 0; i < escapes.length; i++)
            in = in.replace(escapes[i][0], escapes[i][1]);

        return in;
    }

    private String unescape(String in) {

        for (int i = escapes.length - 1; i > -1; i--)
            in = in.replace(escapes[i][1], escapes[i][0]);

        return in;
    }

    private KV to_entry(String data) {
        try {
            String key, value;
            String[] parsed = data.split(":", 2);

            key = unescape(parsed[0].trim());
            value = unescape(parsed[1].trim());

            return new KV(key, value);
        } catch (Exception ex) {
            throw new RuntimeException("Ошибка при парсинге строки", ex);
        }
    }

    private String to_string(Map.Entry<String, String> data) {
        return escape(data.getKey()) + " : " + escape(data.getValue());
    }


    public void save(String namespace, Map<String, String> data) {

        File file = get_data_file(namespace);

        try {

            if (file.exists() && !file.delete())
                throw new RuntimeException("Cannot delete file: " + file.getAbsoluteFile());
            if (!file.createNewFile())
                throw new RuntimeException("Cannot create file: " + file.getAbsoluteFile());

            BufferedWriter data_writer = new BufferedWriter(new FileWriter(file));

            int flow = 0;
            for (Map.Entry<String, String> e : data.entrySet()) {
                data_writer.write(to_string(e) + "\n");
                if (flow++ > 50) {
                    data_writer.flush();
                    flow = 0;
                }
            }
            data_writer.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    public Map<String, String> getData(String namespace) {
        try {
            File data_file = get_data_file(namespace);

            if (!data_file.exists())
                return new HashMap<>();

            BufferedReader in = new BufferedReader(new FileReader(data_file));
            HashMap<String, String> data = new HashMap<>();

            String tmp;
            while ((tmp = in.readLine()) != null) {
                if (tmp.startsWith("#")) continue;
                if (tmp.isEmpty()) continue;
                KV kv = to_entry(tmp);
                data.put(kv.k, kv.v);
            }

            return data;

        } catch (Exception e) {
            throw new RuntimeException("Could not parse file.", e);
        }
    }

}
