package com.cab404.neurogame.settings;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cab404
 */
public class SettingsUtil {

    private static HashMap<String, String> class_aliases;
    static {
        class_aliases = new HashMap<>();
        class_aliases.put("C", "com.badlogic.gdx.graphics.Color");
        class_aliases.put("F", "java.lang.Float");
        class_aliases.put("I", "java.lang.Integer");
        class_aliases.put("B", "java.lang.Boolean");
        class_aliases.put("S", "java.lang.String");
    }

    private static String objectToString(Object object) {
        String class_name = object.getClass().getName();
        return class_name + "@" + object.toString();
    }

    private static Object stringToObject(String str) {
        String[] parsed = str.split("@", 2);

        try {

            String class_name = parsed[0].trim();

            if (class_aliases.containsKey(class_name))
                class_name = class_aliases.get(class_name);


            Class cls = Class.forName(class_name);
            for (Method method : cls.getMethods()) {
                String name = method.getName();
                try {
                    if (name.startsWith("parse") || name.equals("valueOf")) {
                        if (Arrays.equals(method.getParameterTypes(), new Class[]{String.class}) ||
                                Arrays.equals(method.getParameterTypes(), new Class[]{Object.class}))
                            if (method.getReturnType().isAssignableFrom(cls))
                                return method.invoke(null, parsed[1].trim());
                    }

                } catch (NullPointerException ex) {
                    // That's not a static method.
                }
            }

        } catch (Exception ex) {
            throw new RuntimeException("Problems while loading settings at string " + str, ex);
        }

        return null;
    }

    public static Map<String, String> loadIntoMap(Object what_to_parse) {
        HashMap<String, String> out = new HashMap<>();

        for (java.lang.reflect.Field field : extractSettings(what_to_parse)) {
            try {
                out.put(field.getName(), objectToString(field.get(what_to_parse)));
            } catch (Exception ex) {
                throw new RuntimeException("Cannot load field " + field.getName(), ex);
            }
        }

        return out;
    }

    public static void loadFromMap(Object into, Map<String, String> settings) {
        for (java.lang.reflect.Field field : extractSettings(into)) {
            try {
                Object found = stringToObject(settings.get(field.getName()));
                field.set(into, found);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("No access to " + field.getName() + ", but it is listed. WTF.", e);
            }

        }
    }

    public static ArrayList<Field> extractSettings(Object obj) {
        ArrayList<Field> extracted = new ArrayList<>();

        for (java.lang.reflect.Field field : obj.getClass().getFields()) {
            if (field.isAnnotationPresent(SettingsField.class)) {
                extracted.add(field);
            }
        }

        return extracted;
    }

}
