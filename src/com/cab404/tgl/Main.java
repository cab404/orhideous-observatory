package com.cab404.tgl;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.cab404.neurogame.settings.SettingsSaver;
import com.cab404.neurogame.settings.SettingsUtil;
import com.cab404.tgl.scenes.ActivityMode;
import com.cab404.tgl.scenes.PostsMode;
import com.cab404.tgl.scenes.TabunGL;
import com.cab404.tgl.scenes.UsersMode;
import com.cab404.tgl.web.DataThread;

import java.io.File;
import java.util.Map;

/**
 * @author cab404
 */
public class Main {
    public static void main(String[] args) {
        SettingsSaver settingsSaver = new SettingsSaver(new File("."));
        Vars inst = Vars.inst();

        Map<String, String> data = settingsSaver.getData("settings.cfg");
        if (data.isEmpty()) {
            data = SettingsUtil.loadIntoMap(inst);
            settingsSaver.save("settings.cfg", data);
        }
        SettingsUtil.loadFromMap(inst, data);

        TabunGL tabunGL;
        switch (Vars.inst().LAUNCH_MODE) {
            case 1:  /***/tabunGL = new UsersMode();    /***/break;
            case 2:  /***/tabunGL = new ActivityMode(); /***/break;
            case 3:  /***/tabunGL = new PostsMode();    /***/break;
            default: /***/tabunGL = new UsersMode();    /***/break;
        }

        new DataThread(tabunGL).start();

        new LwjglApplication(tabunGL);
    }
}
