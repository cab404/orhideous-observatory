package com.cab404.tgl;

import com.cab404.guidy.AbstractObjectStorage;
import com.cab404.guidy.GameObj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author cab404
 */
public class SimpleObjectStorage extends AbstractObjectStorage {
    private ArrayList<GameObj> data = new ArrayList<>();

    @Override public void remove(GameObj... remove) {
        super.remove(remove);
        data.removeAll(Arrays.asList(remove));
    }

    @Override public void add(GameObj... add) {
        super.add(add);
        data.addAll(Arrays.asList(add));
    }

    @Override
    public Collection<GameObj> getAll() {
        return data;
    }

    @Override public void internalUpdate(float time) {
        Collection<GameObj> objects = getAll();

        for (int i = 0; i < data.size(); ) {
            GameObj upd = data.get(i);

            if (upd.isAlive) {
                upd.update(time);
            } else {
                upd.onDeath();
                objects.remove(upd);
                continue;
            }
            i++;
        }

    }
}
