package com.cab404.tgl;

import com.badlogic.gdx.graphics.Color;
import com.cab404.libtabun.data.StreamItem;
import com.cab404.neurogame.settings.SettingsField;

/**
 * @author cab404
 */
public class Vars {
    private static Vars inst;
    public static Vars inst() {
        if (inst == null) inst = new Vars();
        return inst;
    }

    @SettingsField
    public float
            ZOOM = 1f,                  // 0.01..inf:     Приближение камеры. Чем меньше - тем ближе. Лучше не трогать.
            TTL = 30,                   // 0..inf:        Время жизни не обновляемой планеты.
            SPEED_MOD = 300,            // 1..inf:        Модификатор скорости вращения планет.
            PARTICLES = 1f,             // 0..inf:        Количество частиц в обновление, ~N*60 в секунду на каждого.
            PARTICLE_LIFETIME = 5f,     // 0..inf:        Время жизни частиц, в секундах.
            PARTICLE_SPEED = 10f,       // 0..inf:        Множитель скорости частиц.
            PLANET_SIZE = 10f,          // 0..inf:        Размер планеты.
            DELAY = 5;                  // 0.3f..inf      Период обновления в секундах.
    @SettingsField
    public int
            FONT_SIZE = 10,              // 2..inf:        Размер шрифта.
            LAUNCH_MODE = 1;             // 1/2            Вариант запуска.

    @SettingsField
    public boolean
            TITLES = true;

    @SettingsField
    public Color
            ADD_COMMENT_COLOR = Color.valueOf("5670bd"),
            ADD_TOPIC_COLOR = Color.valueOf("56c5bd"),
            VOTE_BLOG_COLOR = Color.valueOf("806de5"),
            VOTE_TOPIC_COLOR = Color.valueOf("7683f9"),
            VOTE_COMMENT_COLOR = Color.valueOf("c36de5"),
            VOTE_USER_COLOR = Color.valueOf("fc6de5"),
            JOIN_BLOG_COLOR = Color.valueOf("8f3e82"),
            CORE_COLOR = Color.valueOf("FFE552");

    public Color getColor(StreamItem.Type type) {
        switch (type) {
            case ADD_COMMENT:  /***/return ADD_COMMENT_COLOR;
            case ADD_TOPIC:    /***/return ADD_TOPIC_COLOR;
            case VOTE_BLOG:    /***/return VOTE_BLOG_COLOR;
            case VOTE_TOPIC:   /***/return VOTE_TOPIC_COLOR;
            case VOTE_COMMENT: /***/return VOTE_COMMENT_COLOR;
            case VOTE_USER:    /***/return VOTE_USER_COLOR;
            case JOIN_BLOG:    /***/return JOIN_BLOG_COLOR;
            default:           /***/return Color.WHITE.cpy();
        }
    }
}
