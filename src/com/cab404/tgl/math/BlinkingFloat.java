package com.cab404.tgl.math;

/**
 * Yup.
 *
 * @author cab404
 */
public class BlinkingFloat {
    float speed;
    float max, min;
    float value;
    boolean down;

    public BlinkingFloat(float max, float min) {
        this.max = max;
        this.min = min;
    }

    public BlinkingFloat() {}

    public float get() {
        return value;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void update(float dt) {
        float change = speed * dt;

        value += (down ? -1 : 1) * change;
        if (value < min) {
            value = min;
            down = false;
        }

        if (value > max) {
            value = max;
            down = true;
        }

    }


}
