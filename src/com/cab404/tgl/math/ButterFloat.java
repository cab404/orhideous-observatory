package com.cab404.tgl.math;

/**
 * Butter-y changing float.
 *
 * @author cab404
 */
public class ButterFloat {
    float speed = 1;

    float new_value = 0;
    float value = 0;

    public ButterFloat(float start) {
        fset(start);
    }

    public ButterFloat() {}

    public void set(float val) {
        new_value = val;
    }

    public void fset(float val) {
        new_value = value = val;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float get() {
        return value;
    }

    public void update(float dt) {
        float change = Math.signum(new_value - value) * speed * dt;
        change = Math.abs(new_value - value) < Math.abs(change) ? (new_value - value) : change;

        value += change;
    }

}
