package com.cab404.tgl.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.GameObj;
import com.cab404.guidy.Luna;
import com.cab404.tgl.Vars;
import com.cab404.tgl.math.ButterFloat;

/**
 * @author cab404
 */
public class Particle extends GameObj {
    ButterFloat life;
    float val;
    Sprite particle;
    Vector2 vec;
    private Color color;
    BitmapFont font;

    public Particle(Color color) {
        this.color = color;
        val = (float) (Math.random() * 0.5f) + Vars.inst().PARTICLE_LIFETIME;
        life = new ButterFloat(0);
        life.setSpeed(50);
        life.set(val);
        size.set(3, 3);

        vec = new Vector2(
                (float) (Math.random() - 0.5f),
                (float) (Math.random() - 0.5f)
        );

        particle = Luna.load("assets/images/particle.png");
        particle.setSize(5, 5);
    }

    @Override public void render(SpriteBatch batch) {
        float scale = life.get() / val;
        particle.setSize(size.x * scale + 1, size.y * scale + 1);
        particle.setColor(color.r, color.g, color.b, color.a * (life.get() > 1 ? 1 : life.get()));
        particle.setPosition(pos.x - particle.getWidth() / 2, pos.y - particle.getHeight() / 2);
        particle.draw(batch);
    }

    @Override public void update(float time) {
        life.update(time);
        pos.add(vec.cpy().scl(time * Vars.inst().PARTICLE_SPEED));
        if (life.get() == val) {
            life.set(0);
            life.setSpeed(1);
        }
        if (life.get() == 0) isAlive = false;
    }
}
