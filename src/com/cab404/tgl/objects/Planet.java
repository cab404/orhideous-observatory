package com.cab404.tgl.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.GameObj;
import com.cab404.guidy.Luna;
import com.cab404.tgl.Vars;
import com.cab404.tgl.math.BlinkingFloat;
import com.cab404.tgl.math.ButterFloat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author cab404
 */
public class Planet extends GameObj {
    public boolean infinite = false;

    // Time to live
    private ButterFloat ttl;
    private BlinkingFloat scale_x, scale_y;

    // Ассоциированные планеты.
    private HashMap<GameObj, Vector2> resolved;

    private Sprite spr;
    private BitmapFont font;
    private Color color;
    private String name;

    public Planet(Color color, String name) {
        this.color = color;
        this.name = name;
        resolved = new HashMap<>();
        ttl = new ButterFloat();
        ttl.fset(0);
        ttl.set(Vars.inst().TTL);

        font = Luna.font();
        scale_x = new BlinkingFloat(1, 0.9f);
        scale_y = new BlinkingFloat(1, 0.9f);
        scale_x.setSpeed(0.07f);
        scale_y.setSpeed(0.03f);

        spr = Luna.load("assets/images/circle.png");
    }

    public void resolve(GameObj planet, float len) {
        Vector2 res_vec = new Vector2(0, len);
        res_vec.rotate((float) (Math.random() * 360));
        resolved.put(planet, res_vec);
    }

    @Override public void render(SpriteBatch batch) {
        color.a = ttl.get();
        color.clamp();

        spr.setSize(size.x * scale_x.get(), size.y * scale_y.get());
        spr.setPosition(pos.x - spr.getWidth() / 2, pos.y - spr.getHeight() / 2);
        spr.setColor(color);
        spr.draw(batch);

        if (Vars.inst().TITLES) {
            font.setColor(color);
            font.drawMultiLine(batch, name, pos.x + spr.getWidth() / 2, pos.y + spr.getHeight() / 2);
        }

    }

    public void kill() {
        ttl.fset(1);
        ttl.set(0);
        resolved.clear();
    }

    @Override public void update(float time) {

        if (resolved.size() != 0 || infinite)
            ttl.set(Vars.inst().TTL);
        else if (ttl.get() == Vars.inst().TTL)
            ttl.set(0);

        ttl.update(time);

        scale_x.update(time);
        scale_y.update(time);

        if (ttl.get() == 0) {
            isAlive = false;
            return;
        }

        Iterator<Map.Entry<GameObj, Vector2>> resolve = resolved.entrySet().iterator();
        while (resolve.hasNext()) {
            Map.Entry<GameObj, Vector2> proc = resolve.next();
            GameObj obj = proc.getKey();
            Vector2 vec = proc.getValue();

            if (!obj.isAlive) {
                resolve.remove();
                continue;
            }

            vec.rotate((size.x * size.y * Vars.inst().SPEED_MOD) / vec.dst2(0, 0) * time);

            obj.pos.set(pos.cpy().add(vec));
        }

        float particles = (float) (Math.random() * Vars.inst().PARTICLES);
        while (particles > 0) {
            particles -= Math.random();
            if (particles < 0) continue;

            Particle particle = new Particle(Luna.random_tint(color.cpy().mul(1, 1, 1, 0.5f)));
            Vector2 ps = new Vector2((float) (size.x / 2 * Math.random()), 0);
            ps.rotate((float) (Math.random() * 360));
            particle.size.set(size.cpy().scl(0.05f));
            particle.pos.set(pos.cpy().add(ps));
            parent.add(particle);
        }

    }

}
