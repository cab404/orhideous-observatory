package com.cab404.tgl.scenes;

import com.badlogic.gdx.graphics.Color;
import com.cab404.libtabun.data.StreamItem;
import com.cab404.tgl.Vars;
import com.cab404.tgl.objects.Planet;

/**
 * @author cab404
 */
public class ActivityMode extends TabunGL {

    @Override
    public void synchronized_handle(StreamItem item) {
        Color p_col = Vars.inst().getColor(item.type);

        Planet planet = new Planet(p_col, item.user.nick + ":\n " + item.data + "\n");
        planet.size.set(Vars.inst().PLANET_SIZE, Vars.inst().PLANET_SIZE);
        tabun.resolve(planet, (float) Math.random() * (h / 2 - 50) + 50);
        field.add(planet);

    }
}
