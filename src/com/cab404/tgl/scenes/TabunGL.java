package com.cab404.tgl.scenes;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Texture;
import com.cab404.guidy.FL;
import com.cab404.guidy.Luna;
import com.cab404.guidy.SL;
import com.cab404.libtabun.data.StreamItem;
import com.cab404.tgl.SimpleObjectStorage;
import com.cab404.tgl.Vars;
import com.cab404.tgl.objects.Planet;
import com.cab404.tgl.web.DataThread;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cab404
 */
public abstract class TabunGL implements ApplicationListener, DataThread.DataHandler {
    protected int w, h;
    protected int mdisp;
    protected SimpleObjectStorage field;
    protected SimpleObjectStorage ui;
    protected final List<StreamItem> data;
    protected Planet tabun;

    public TabunGL() {
        data = new ArrayList<>();
    }

    @Override public void create() {
        Texture.setEnforcePotImages(false);
        field = new SimpleObjectStorage();
        ui = new SimpleObjectStorage();
        FL.getInstance().loadFont(Gdx.files.internal("assets/fonts/Ubuntu-L.ttf"), FL.DEFAULT, Vars.inst().FONT_SIZE);

        tabun = new Planet(Vars.inst().CORE_COLOR, "Табун");
        tabun.infinite = true;
        tabun.pos.set(400, 400);
        tabun.size.set(50, 50);

        field.add(tabun);

    }

    @Override public void resize(int w, int h) {
        this.w = w;
        this.h = h;

        mdisp = Math.min(w, h);
        ui.gfx.update();
        field.gfx.update();
    }


    float last = 0;

    //    U.Timer timer = new U.Timer();
    @Override public void render() {
//        if (timer.getMs() > 10000) {
//            U.v(field.data.size() + " particles : " + Gdx.graphics.getFramesPerSecond() + " fps");
//            timer.set();
//        }

        // Update
        ui.update(Luna.dt());
        field.update(Luna.dt());
        last += Luna.dt();

        field.gfx.cam.position.x = tabun.pos.x - mdisp / 2;
        field.gfx.cam.position.y = tabun.pos.y - mdisp / 2;
        field.gfx.cam.zoom = Vars.inst().ZOOM;
        field.gfx.update();

        synchronized (data) {
            if (data.size() > 0) {
                if (last > Vars.inst().DELAY / data.size()) {
                    synchronized_handle(data.remove(0));
                    last = 0;
                }
            }
        }

        // Render
        Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(0, 0, 0.05f, 1);

        ui.render();
        field.render();

    }

    @Override public void pause() {}

    @Override public void resume() {}

    @Override public void dispose() {
        SL.inst().dispose();
    }

    public abstract void synchronized_handle(StreamItem item);

    @Override public void handle(StreamItem item) {
        synchronized (data) {
            data.add(item);
        }
    }
}
