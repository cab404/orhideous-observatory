package com.cab404.tgl.scenes;

import com.cab404.libtabun.data.StreamItem;
import com.cab404.moonlight.util.SU;
import com.cab404.tgl.Vars;
import com.cab404.tgl.objects.Planet;

import java.util.HashMap;
import java.util.List;

/**
 * @author cab404
 */
public class UsersMode extends TabunGL {

    HashMap<String, Planet>
            users = new HashMap<>(),
            posts = new HashMap<>(),
            blogs = new HashMap<>();

    @Override public void synchronized_handle(StreamItem item) {
        if (item.type == StreamItem.Type.JOIN_BLOG) return;
        if (item.type == StreamItem.Type.VOTE_BLOG) return;
        if (item.type == StreamItem.Type.VOTE_USER) return;
        if (item.type == null) return;
        if (!item.link.startsWith("http://tabun.everypony.ru/blog/")) return;

        float size = Vars.inst().PLANET_SIZE;

        List<String> data = SU.charSplit(item.link.substring("http://tabun.everypony.ru/blog/".length()), '/');
        String post_name = item.data.trim();
        String blog_name = SU.charSplit(data.get(0).trim(), '#').get(0);
        String user_name = item.user.nick.trim();

        Planet
                blog = blogs.get(blog_name),
                post = posts.get(post_name),
                user = users.get(user_name);

        if (blog == null || !blog.isAlive) {
            blog = new Planet(Vars.inst().VOTE_BLOG_COLOR, blog_name);
            blog.size.set(size * 2, size * 2);
            tabun.resolve(blog, (float) (Math.random() * (mdisp / 2 - 70) + 70));
            field.add(blog);
        }

        if (post == null || !post.isAlive) {
            post = new Planet(Vars.inst().VOTE_TOPIC_COLOR, post_name);
            post.size.set(size, size);
            blog.resolve(post, (float) (Math.random() * size + size));
            field.add(post);
        }

        if (user != null)
            user.kill();
        user = new Planet(Vars.inst().VOTE_USER_COLOR, user_name);
        user.size.set(size / 2, size / 2);
        field.add(user);
        post.resolve(user, (float) (Math.random() * size / 2 + size / 2));

        blogs.put(blog_name, blog);
        posts.put(post_name, post);
        users.put(user_name, user);

    }
}
