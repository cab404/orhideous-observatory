package com.cab404.tgl.web;

import com.cab404.libtabun.data.StreamItem;
import com.cab404.libtabun.pages.StreamPage;
import com.cab404.moonlight.facility.MessageFactory;
import com.cab404.moonlight.framework.AccessProfile;
import com.cab404.tgl.Vars;

/**
 * @author cab404
 */
public class DataThread extends Thread {

    private DataHandler handler;
    public DataThread(DataHandler handler) {
        super();
        this.handler = handler;
        setDaemon(true);
    }

    @Override public void run() {
        AccessProfile profile = new AccessProfile("tabun.everypony.ru");
        MessageFactory.impl = new MessageFactory.MessageListener() {
            @Override public void show(String title, String body, boolean isError) {
            }
        };

        StreamItem item = new StreamItem();
        while (true) {

            StreamPage streamPage = new StreamPage();
            streamPage.fetch(profile);

            for (StreamItem str : streamPage.stream)
                if (!str.link.equals(item.link)) {
                    handler.handle(str);
                } else
                    break;
            item = streamPage.stream.get(0);


            try {
                Thread.sleep((long) (Vars.inst().DELAY * 1000));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static interface DataHandler {
        public void handle(StreamItem item);
    }

}
